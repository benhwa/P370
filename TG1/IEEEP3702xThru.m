% Copyright 2017 The Institute of Electrical and Electronics Engineers, Incorporated (IEEE).
%
% This work is licensed to The Institute of Electrical and Electronics
% Engineers, Incorporated (IEEE) under one or more contributor license
% agreements.
%
% See the NOTICE file distributed with this work for additional
% information regarding copyright ownership. Use of this file is
% governed by a BSD-style license, the terms of which are as follows:
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions
% are met:
%
%   * Redistributions of source code must retain the above copyright
%   notice, this list of conditions, the following disclaimer, and the
%   NOTICE file.
%
%   * Redistributions in binary form must reproduce the above
%   copyright notice, this list of conditions, the following
%   disclaimer in the documentation and/or other materials provided
%   with the distribution, and the NOTICE file.
%
%   * Neither the name of The Institute of Electrical and Electronics
%   Engineers, Incorporated (IEEE) nor the names of its contributors
%   may be used to endorse or promote products derived from this
%   software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
% FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
% COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
% INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
% BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
% LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
% ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
%
% SPDX-License-Identifier: BSD-3-Clause

% Function: [s_side1,s_side2] = IEEEP3702xThru(s_2xthru)
%
% Author: Jason J. Ellison, Published March 24th, 2017
% 
% IEEEP3702xThru.m creates error boxes from a test fixture 2x thru. 
% 
% Input: 
% s_2xthru--an s parameter object of the 2x thru.
% 
% Outputs: 
% s_side1--an s parameter object of the error box representing the half of the 2x thru connected to port 1
% s_side2--an s parameter object of the error box representing the half of the 2x thru connected to port 2
% 
% residual test usage:
% 
% [e1,e2] = IEEEP3702xThru(s);
% s_residual = deembedsparams(s,e1,e2);

function [s_side1,s_side2] = IEEEP3702xThru(s_2xthru)

s2x = s_2xthru;
f = s2x.Frequencies;
n = length(f);
npad = n*2+1;
df = f(2)-f(1);
t = linspace(0,1/df,npad);
p2x = s2x.Parameters;
p112x = squeeze(p2x(1,1,:));
p212x = squeeze(p2x(2,1,:));

% get the DC point of p112x and p212x and zero pad
p112xDC = 0;
p112x = [p112xDC; p112x];

y = log(abs(p212x(1:5)));
x = [ones(5,1) sqrt(f(1:5))];
[b] = (x'*x)\x'*y;
p212xDC = exp(b(1));
p212x = [p212xDC; p212x];

p112x = makeSymmetric(p112x);
p212x = makeSymmetric(p212x);

% convert p112x and p212x to time
t112x = ifft(p112x,'symmetric');
t212x = ifft(p212x,'symmetric');

% convert t112x and t212x to step response
t112xStep = zeros(npad,1);
for num = 1:npad
    t112xStep(num) = sum(t112x(1:num));
end

t212xStep = zeros(npad,1);
for num = 1:npad
    t212xStep(num) = sum(t212x(1:num));
end

% find the mid point of the trace in z
% figure; plot(t,t212xStep);
[~,mid_z] = min(abs(t212xStep-0.5));

% make the t112xStep t111xStep
t111xStep = t112xStep;
out{1} = t111xStep(1);
t111xStep(mid_z:end) = t111xStep(1);
t111xStep = t111xStep - t111xStep(1); % kinda renormalization

% convert t111xStep to t111x
t111x = t111xStep - [0 ; t111xStep(1:end-1)];

% convert to p111x
p111x = fft(t111x);


% remove the DC points and convert back to original frequency
p111x = p111x(2:n + 1);
p112x = p112x(2:n + 1);
p212x = p212x(2:n + 1);

% calculate the p221x and p211x
p221x = (p112x - p111x)./p212x;

% the choice of sign in the sqrt is important. Use the sign that captures the proper angle.
p211x = zeros(n,1);
p211x(1) = sqrt(p212x(1).*(1-p221x(1).^2));
test = sqrt(p212x.*(1-p221x.^2));
k = 1;
for i = 2:n
    if angle(test(i)) - angle(test(i-1)) > 0
        k = k*-1;
    end
    p211x(i) = k*sqrt(p212x(i).*(1-p221x(i).^2));
end

% create the error box and make the s-parameter block
errorbox = zeros(2,2,n);
errorbox(1,1,:) = p111x;
errorbox(2,1,:) = p211x;
errorbox(1,2,:) = p211x;
errorbox(2,2,:) = p221x;
z0 = 50;
zl1 = -z0*(out{1} + 1)/(out{1} - 1);
s_side1 = sparameters(errorbox,f,zl1);

F = zeros(2,2,length(f));
F(1,1,:) = sqrt(z0/z0);
F(2,2,:) = sqrt(zl1/z0);
z_side1_new = zeros(2,2,length(f));


p_side1 = s_side1.Parameters;
z_side1 = s2z(p_side1,zl1);
for i = 1:length(f)
    z_side1_new(:,:,i) = F(:,:,i)*z_side1(:,:,i)*F(:,:,i);
end
p_side1_new = z2s(z_side1_new,z0);
s_side1 = sparameters(p_side1_new,f,z0);

p2x = s2x.Parameters;
p112x = squeeze(p2x(2,2,:));
p212x = squeeze(p2x(1,2,:));

% get the DC point of p112x and p212x and zero pad
p112xDC = 0;
p112x = [p112xDC; p112x];

y = log(abs(p212x(1:5)));
x = [ones(5,1) sqrt(f(1:5))];
[b] = (x'*x)\x'*y;
p212xDC = exp(b(1));
p212x = [p212xDC; p212x];

p112x = makeSymmetric(p112x);
p212x = makeSymmetric(p212x);

% convert p112x and p212x to time
t112x = ifft(p112x,'symmetric');
t212x = ifft(p212x,'symmetric');

% convert t112x and t212x to step response
t112xStep = zeros(npad,1);
for num = 1:npad
    t112xStep(num) = sum(t112x(1:num));
end


t212xStep = zeros(npad,1);
for num = 1:npad
    t212xStep(num) = sum(t212x(1:num));
end

% find the mid point of the trace in z
[~,mid_z] = min(abs(t212xStep-0.5));

% make the t112xStep t111xStep
t111xStep = t112xStep;
out{2} = t111xStep(1);
t111xStep(mid_z:end) = t111xStep(1);
% t111xStep = t111xStep - t111xStep(1);

% convert t111xStep to t111x
t111x = t111xStep - [0 ; t111xStep(1:end-1)];
t111x(1) = t111x(end);

% convert to p111x
p111x = fft(t111x);

% remove the DC points and convert back to original frequency
p111x = p111x(2:n + 1);
p112x = p112x(2:n + 1);
p212x = p212x(2:n + 1);

% calculate the p221x and p211x
p221x = (p112x - p111x)./p212x;

% the choice of sign in the sqrt is important. Use the sign that captures the proper angle.
p211x = zeros(n,1);
p211x(1) = sqrt(p212x(1).*(1-p221x(1).^2));
test = sqrt(p212x.*(1-p221x.^2));
k = 1;
for i = 2:n
    if angle(test(i)) - angle(test(i-1)) > 0
        k = k*-1;
    end
    p211x(i) = k*sqrt(p212x(i).*(1-p221x(i).^2));
end

% create the error box and make the s-parameter block
errorbox = zeros(2,2,n);
errorbox(1,1,:) = p111x;
errorbox(2,1,:) = p211x;
errorbox(1,2,:) = p211x;
errorbox(2,2,:) = p221x;
z0 = 50;
zl2 = -z0*(out{2} + 1)/(out{2} - 1);
s_side2 = sparameters(errorbox([2 1],[2 1],:),f,zl2);

F = zeros(2,2,length(f));
F(1,1,:) = sqrt(zl2/z0);
F(2,2,:) = sqrt(z0/z0);
z_side2_new = zeros(2,2,length(f));


p_side2 = s_side2.Parameters;
z_side2 = s2z(p_side2,zl2);
for i = 1:length(f)
    z_side2_new(:,:,i) = F(:,:,i)*z_side2(:,:,i)*F(:,:,i);
end
p_side2_new = z2s(z_side2_new,z0);
s_side2 = sparameters(p_side2_new,f,z0);

function [symmetric] = makeSymmetric(nonsymmetric)
% [symmetric] = makeSymmetric(nonsymmetric)
% this takes the nonsymmetric frequency domain input and makes it
% symmetric.
%
% The function assumes the DC point is in the nonsymmetric data

% symmetric_abs = [abs(nonsymmetric);flip(abs(nonsymmetric(2:end)))]/2;
symmetric_abs = [abs(nonsymmetric); flip(abs(nonsymmetric(2:end)))];
symmetric_ang = [angle(nonsymmetric); -flip(angle(nonsymmetric(2:end)))];
symmetric = symmetric_abs.*exp(1i.*symmetric_ang);