% Copyright 2017 The Institute of Electrical and Electronics Engineers, Incorporated (IEEE).
%
% This work is licensed to The Institute of Electrical and Electronics
% Engineers, Incorporated (IEEE) under one or more contributor license
% agreements.
%
% See the NOTICE file distributed with this work for additional
% information regarding copyright ownership. Use of this file is
% governed by a BSD-style license, the terms of which are as follows:
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions
% are met:
%
%   * Redistributions of source code must retain the above copyright
%   notice, this list of conditions, the following disclaimer, and the
%   NOTICE file.
%
%   * Redistributions in binary form must reproduce the above
%   copyright notice, this list of conditions, the following
%   disclaimer in the documentation and/or other materials provided
%   with the distribution, and the NOTICE file.
%
%   * Neither the name of The Institute of Electrical and Electronics
%   Engineers, Incorporated (IEEE) nor the names of its contributors
%   may be used to endorse or promote products derived from this
%   software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
% FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
% COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
% INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
% BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
% LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
% ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
%
% SPDX-License-Identifier: BSD-3-Clause

% Function: [s_side1,s_side2] = IEEEP3701xOpen(s_open)
%
% Author: Jason J. Ellison, Published July 7th, 2017
%
% Revision: v0

% 
% IEEEP3701xOpen.m creates error boxes from a test fixture 1x open. 
% 
% Input: 
% s_open--an s parameter object of the 1x open.
% 
% Outputs: 
% s_side1--an s parameter object of the error box representing the test fixture connected to port 1
% s_side2--an s parameter object of the error box representing the test fixture connected to port 2
% 
% residual test usage:
% 
% [e1,e2] = IEEEP3701xOpen(s_2xthru);
% s_residual = deembedsparams(s,e1,e2);

function [s_side1,s_side2] = IEEEP3701xOpen(s_open)
p = s_open.Parameters;
f = s_open.Frequencies;
n = length(f);

% open analysis
s11 = squeeze(p(1,1,:));
s11 = makeSymmetric([0;s11]); % using DC = 0;

% convert to time and view time domain
t = linspace(0,1/(f(2) - f(1)),n*2 + 1);
t11 = ifft(s11,'symmetric');

% convert the impulse to step
t11step = zeros(n*2+1,1);
for num = 1:n*2+1
    t11step(num) = sum(t11(1:num));
end

% find the first point of the open
for i = 1:n*2+1
    if t11step(i) >= 0.2
        stop = i-3;
        break;
    end
end

% make the e00 term
% make the t11step 0 after the final point of the open
t11step_e00 = zeros(n*2+1,1);
t11step_e00(1:stop) = t11step(1:stop);
% t11step_e00_t = t11step_e00(1:stop);
% t_temp = t(1:stop).'; % this is a truncated time vector
% t_ = [ones(stop,1) t_temp.^8]; % this is the basis for the detrending
% T = inv(t_'*t_)*t_'*t11step_e00_t;
% trend = T(2).*t_temp.^8;
% t11step_e00(1:stop) = t11step_e00(1:stop) - trend; % detrending

% change e00 time step to time impulse then frequency domain
t11_e00 = t11step_e00 - [0 ; t11step_e00(1:end-1)];
e00 = fft(t11_e00);
temp = e00(2:n+1); % convert to original frequency
clear e00;
e00 = temp; clear temp;
temp = s11(2:n+1); % convert to original frequency
clear s11;
s11 = temp; clear temp;
e11 = e00;

% calculate e01
e01sqr = (1 - e11).*(s11 - e00);
e01_angle = unwrap(angle(e01sqr))./2;
e01_abs = sqrt(abs(e01sqr));
e01 = e01_abs.*exp(1i.*e01_angle);

% calculate error box
error_box1 = zeros(2,2,n);
error_box1(1,1,:) = e00;
error_box1(1,2,:) = e01;
error_box1(2,1,:) = e01;
error_box1(2,2,:) = e11;
s_side1 = sparameters(error_box1,f);
error_box2 = error_box1([2 1],[2 1],:);
s_side2 = sparameters(error_box2,f);

% adjust phase for odd behavior of this technique
delayline = rfckt.delay('Z0',50,'TimeDelay',1/f(end));
analyze(delayline,f);
t_delay = s2t(delayline.AnalyzedResult.S_Parameters);

% - side 1
t_side1 = s2t(s_side1.Parameters);
t_side1_ = zeros(2,2,length(f));
for i = 1:length(f)
    t_side1_(:,:,i) = t_side1(:,:,i)/t_delay(:,:,i);
end
s_side1_ = t2s(t_side1_);
s_side1 = sparameters(s_side1_,f);

% - side 2
t_side2 = s2t(s_side2.Parameters);
t_side2_ = zeros(2,2,length(f));
for i = 1:length(f)
    t_side2_(:,:,i) = t_delay(:,:,i)\t_side2(:,:,i);
end
s_side2_ = t2s(t_side2_);
s_side2 = sparameters(s_side2_,f);


function [symmetric] = makeSymmetric(nonsymmetric)
% [symmetric] = makeSymmetric(nonsymmetric)
% this takes the nonsymmetric frequency domain input and makes it
% symmetric.
%
% The function assumes the DC point is in the nonsymmetric data

symmetric_abs = [abs(nonsymmetric); flip(abs(nonsymmetric(2:end)))];
symmetric_ang = [angle(nonsymmetric); -flip(angle(nonsymmetric(2:end)))];
symmetric = symmetric_abs.*exp(1i.*symmetric_ang);