% Copyright 2017 The Institute of Electrical and Electronics Engineers, Incorporated (IEEE).
%
% This work is licensed to The Institute of Electrical and Electronics
% Engineers, Incorporated (IEEE) under one or more contributor license
% agreements.
%
% See the NOTICE file distributed with this work for additional
% information regarding copyright ownership. Use of this file is
% governed by a BSD-style license, the terms of which are as follows:
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions
% are met:
%
%   * Redistributions of source code must retain the above copyright
%   notice, this list of conditions, the following disclaimer, and the
%   NOTICE file.
%
%   * Redistributions in binary form must reproduce the above
%   copyright notice, this list of conditions, the following
%   disclaimer in the documentation and/or other materials provided
%   with the distribution, and the NOTICE file.
%
%   * Neither the name of The Institute of Electrical and Electronics
%   Engineers, Incorporated (IEEE) nor the names of its contributors
%   may be used to endorse or promote products derived from this
%   software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
% FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
% COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
% INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
% BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
% LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
% ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
%
% SPDX-License-Identifier: BSD-3-Clause

function [s_side1,s_side2] = IEEEP3701xReflect(s_open,s_short)
p_open = s_open.Parameters;
p_short = s_short.Parameters;
f = s_open.Frequencies;
n = length(f);

s11open = squeeze(p_open(1,1,:));
s11open_ = s11open;

s11short = squeeze(p_short(1,1,:));
s11short_ = s11short;

s11open = [0;s11open];
s11open = makeSymmetric(s11open);

s11short = [0;s11short];
s11short = makeSymmetric(s11short);

% convert to time and view time domain
t = linspace(0,1/(f(2) - f(1)),n*2 + 1);
t11 = ifft(s11open,'symmetric');

% convert the impulse to step
t11step = zeros(n*2 + 1,1);
for num = 1:n*2 + 1
    t11step(num) = sum(t11(1:num));
end

% find the first point of the open
for i = 1:n*2 + 1
    if t11step(i) >= 0.2
        stop = i-3;
        break;
    end
end

% make the e00 term from the open
% make the t11step 0 after the final point of the open
t11step_e00 = zeros(n*2 + 1,1);
t11step_e00(1:stop) = t11step(1:stop);
t11step_e00open = t11step_e00; 

% make the e00 from the short
% convert to time and view time domain
t = linspace(0,1/(f(2) - f(1)),n*2 + 1);
t11 = ifft(s11short,'symmetric');

% convert the impulse to step
t11step = zeros(n*2 + 1,1);
for num = 1:n*2 + 1
    t11step(num) = sum(t11(1:num));
end

% find the first point of the short
for i = 1:n*2 + 1
    if t11step(i) <= -0.2
        stop = i-3;
        break;
    end
end

% make the e00 term

% make the t11step 0 after the final point of the short
t11step_e00 = zeros(n*2 + 1,1);
t11step_e00(1:stop) = t11step(1:stop);
t11step_e00short = t11step_e00;

% convert average to f-domain
t11step_e00 = (t11step_e00open + t11step_e00short)/2;
t11_e00 = t11step_e00 - [0 ; t11step_e00(1:end-1)];
e00 = fft(t11_e00);
temp = e00(2:n+1); % convert to original frequency
clear e00;
e00 = temp; clear temp; 

t11_e00open = t11step_e00open - [0 ; t11step_e00open(1:end-1)];
e00open = fft(t11_e00open);
temp = e00open(2:n+1); % convert to original frequency
clear e00open;
e00open = temp; clear temp;

t11_e00short = t11step_e00short - [0 ; t11step_e00short(1:end-1)];
e00short = fft(t11_e00short);
temp = e00short(2:n+1); % convert to original frequency
clear e00short;
e00short = temp; clear temp;

% calculate e11 
e11 = (2.*(s11open_ - e00open) - (s11open_ - s11short_))./(s11open_ - s11short_);

e01sqr = (s11open_ - e00).*(1 - e11);
e01_angle = unwrap(angle(e01sqr))./2;
e01o = e01_angle;
e01_abs = sqrt(abs(e01sqr));
e01open = e01_abs.*exp(1i.*e01_angle);    

e01sqr = (s11short_ - e00).*(1 + e11);
e01_angle = unwrap(angle(e01sqr))./2;
e01s = e01_angle;
e01_abs = sqrt(abs(e01sqr));
e01short = e01_abs.*exp(1i.*e01_angle); 

adjustment = e01o - e01s;

e01short_mag = abs(e01short); e01short_ang = unwrap(angle(e01short)) - pi/2;
% e01short_mag = abs(e01short); e01short_ang = unwrap(angle(e01short)) + adjustment;
e01open_mag = abs(e01open); e01open_ang = unwrap(angle(e01open));
e01 = ((e01short_mag + e01open_mag)/2).*exp(1i.*((e01open_ang + e01short_ang)./2));


% calculate error box
error_box1 = zeros(2,2,n);
error_box1(1,1,:) = e00;
error_box1(1,2,:) = e01;
error_box1(2,1,:) = e01;
error_box1(2,2,:) = e11;
s_side1 = sparameters(error_box1,f);
error_box2 = error_box1([2 1],[2 1],:);
s_side2 = sparameters(error_box2,f); 

% adjust phase for odd behavior of this technique
delayline = rfckt.delay('Z0',50,'TimeDelay',1/f(end));
analyze(delayline,f);
t_delay = s2t(delayline.AnalyzedResult.S_Parameters);

% - side 1
t_side1 = s2t(s_side1.Parameters);
t_side1_ = zeros(2,2,length(f));
for i = 1:length(f)
    t_side1_(:,:,i) = t_side1(:,:,i)/t_delay(:,:,i);
end
s_side1_ = t2s(t_side1_);
s_side1 = sparameters(s_side1_,f);

% - side 2
t_side2 = s2t(s_side2.Parameters);
t_side2_ = zeros(2,2,length(f));
for i = 1:length(f)
    t_side2_(:,:,i) = t_delay(:,:,i)\t_side2(:,:,i);
end
s_side2_ = t2s(t_side2_);
s_side2 = sparameters(s_side2_,f);

function [symmetric] = makeSymmetric(nonsymmetric)
% [symmetric] = makeSymmetric(nonsymmetric)
% this takes the nonsymmetric frequency domain input and makes it
% symmetric.
%
% The function assumes the DC point is in the nonsymmetric data

symmetric_abs = [abs(nonsymmetric); flip(abs(nonsymmetric(2:end)))];
symmetric_ang = [angle(nonsymmetric); -flip(angle(nonsymmetric(2:end)))];
symmetric = symmetric_abs.*exp(1i.*symmetric_ang);